<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Illuminate\Support\Collection;
use Livewire\Component;

class TaskList extends Component
{
    public string $newTaskDescription = '';

    public function render()
    {
        $tasks = Task::orderByDesc('completed_at')->get();
        list($completedTasks, $tasks) = $tasks->partition('completed_at');
        return view('livewire.task-list',[
            'tasks'=>$tasks,
            'completedTasks'=>$completedTasks,
        ]);
    }

    public function addTask()
    {
        Task::create(['description' => $this->newTaskDescription]);
        $this->newTaskDescription = '';
    }

    public function completeTask(Task $task)
    {
        $task->complete()->save();
    }

    public function uncomplete(Task $task)
    {
        $task->completed_at = null;
        $task->save();
    }
}
