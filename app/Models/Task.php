<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $description
 * @property Carbon $completed_at
 * @mixin Builder
 */
class Task extends Model
{
    use HasFactory;
    protected $fillable = [
        'description',
    ];
    protected $dates = ['completed_at'];

    public function complete()
    {
        $this->completed_at = now();
        return $this;
    }
}
