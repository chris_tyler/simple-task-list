<div class="flex flex-col rounded shadow md:w-2/3 lg:w-1/2 mx-auto max-h-full my-">
    <div class="bg-white p-5 rounded-t flex flex-col">
        <h1 class="font-bold text-xl mb-4">My Tasks</h1>
        <div class="self-center">
            @if($tasks->isNotEmpty())
            <div class="mt-2">
                <h2 class="uppercase font-thin mb-1">To-Do</h2>
                <div class="rounded-lg border">
                    @foreach($tasks as $task)
                        <label wire:target="completeTask" wire:loading.class="cursor-wait" wire:loading.class.remove="cursor-pointer" class="cursor-pointer flex block items-center px-2 py-1{{$loop->last ?'':' border-b'}}">
                            <span class="min-w-80">{{$task->description}}</span>
                            <input type="checkbox" wire:click="completeTask({{$task->id}})">
                        </label>
                    @endforeach
                </div>
            </div>
            @endif
            @if($completedTasks->isNotEmpty())
            <div class="mt-2 text-gray-400  mb-1">
                <h2 class="uppercase">Completed</h2>
                <div class="rounded-lg border">
                    @foreach($completedTasks as $completedTask)
                        <label wire:target="uncomplete" wire:loading.class="cursor-wait" wire:loading.class.remove="cursor-pointer"  wire:click="uncomplete({{$completedTask->id}})" class="cursor-pointer min-w-80 w-full block line-through px-2 py-1{{$loop->last ?'':' border-b'}}">{{$completedTask->description}}</label>
                    @endforeach
                </div>
            </div>
            @endif
        </div>
    </div>
    <form wire:submit.prevent="addTask" class="bg-gray-100 p-5 rounded-b">
        <label for="new-task">New Task</label>
        <fieldset class="flex items-stretch" wire:loading.attr="disabled" wire:target="addTask">
            <input autofocus id="new-task" class="py-1 px-2 border border-2 border-r-0 rounded-l flex-auto" wire:model.lazy="newTaskDescription">
            <button class="border border-2 flex flex-none font-medium items-center pl-6 pr-3 py-1 rounded-r text-sm">
                <svg xmlns="http://www.w3.org/2000/svg" class="mr-3 w-2.5" viewBox="0 0 24 24"><path d="M24 10h-10v-10h-4v10h-10v4h10v10h4v-10h10z"/></svg>
                Add
            </button>
        </fieldset>
    </form>
</div>
